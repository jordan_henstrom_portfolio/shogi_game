//package com.zetcode;

import java.awt.EventQueue;
import javax.swing.JFrame;

public class ShogiGame extends JFrame {

    public ShogiGame() {

        initUI();
    }

    private void initUI() {

        add(new GameBoard());

        setSize(330, 330);

        setTitle("Shogi Game");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    public static void main(String[] args) {

        EventQueue.invokeLater(() -> {
            ShogiGame ex = new ShogiGame();
            ex.setVisible(true);
        });
    }
}
