//package com.zetcode;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import javax.swing.JPanel;
import java.awt.geom.Rectangle2D;

public class GameBoard extends JPanel {

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);

    drawMat(g);
    // drawMenu();
    // drawBoard();
    // drawPieces();
  }

  private void drawMat(Graphics g) {

    Graphics2D g2d = (Graphics2D) g;

    RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

    rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

    g2d.setRenderingHints(rh);

    Dimension s = getSize();
    double w = s.getWidth();
    double h = s.getHeight();
    // System.out.println(w);
    // System.out.println(h);

    Rectangle2D r = new Rectangle2D.Double(0,0,w/2/10,h/10);
    g2d.setStroke(new BasicStroke(1));
    g2d.setColor(Color.blue);

    for (double deg = 0; deg < 10; deg++) {
    AffineTransform at = AffineTransform.getTranslateInstance(w/2, h/2);
    g2d.draw(at.createTransformedShape(r));
    }

    // Rectangle2D red = new Rectangle2D.Double(w/2,0,w/2/10,h/10);
    // g2d.setStroke(new BasicStroke(1));
    // g2d.setColor(Color.red);
    // g2d.draw(red);
  }
}
